################################################################################
# Settings
################################################################################

VENDOR_NAME=agogpixel
APP_NAME=alpine-builder
CR_HOST=registry.gitlab.com
GROUP_PATH=devops/docker

ALPINE_TAGS=3.10 3.11

################################################################################
# Parameters
################################################################################

CR_USER?=guest
CR_PASSWORD?=itsasecrettoeveryone

################################################################################
# Computed Values
################################################################################

GIT_SHORT_COMMIT_HASH=$(shell git diff-index --quiet HEAD && git rev-parse --short HEAD)

IMAGE_NAME=$(VENDOR_NAME)/$(APP_NAME)
CR_IMAGE_NAME=$(CR_HOST)/$(VENDOR_NAME)/$(GROUP_PATH)/$(APP_NAME)

IMAGE_LATEST_SOURCE=$(IMAGE_NAME):$(lastword $(ALPINE_TAGS))

ifeq ($(GIT_SHORT_COMMIT_HASH),)
	IMAGE_LATEST_SOURCE:=$(IMAGE_LATEST_SOURCE)-dirty
endif

################################################################################
# Rules
################################################################################

build: Dockerfile
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker build -t $(IMAGE_NAME):$(ALPINE_TAG) -t $(IMAGE_NAME):$(ALPINE_TAG)-$(GIT_SHORT_COMMIT_HASH) --build-arg ALPINE_TAG=$(ALPINE_TAG) .;)
else
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker build -t $(IMAGE_NAME):$(ALPINE_TAG)-dirty --build-arg ALPINE_TAG=$(ALPINE_TAG) .;)
endif
	docker tag $(IMAGE_LATEST_SOURCE) $(IMAGE_NAME):latest

test: build
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker run --rm -d $(IMAGE_NAME):$(ALPINE_TAG);)
else
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker run --rm -d $(IMAGE_NAME):$(ALPINE_TAG)-dirty;)
endif

release: login test tag
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker push $(CR_IMAGE_NAME):$(ALPINE_TAG); docker push $(CR_IMAGE_NAME):$(ALPINE_TAG)-$(GIT_SHORT_COMMIT_HASH);)
else
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker push $(CR_IMAGE_NAME):$(ALPINE_TAG)-dirty;)
endif
	docker push $(CR_IMAGE_NAME):latest

login:
	docker login -u $(CR_USER) -p $(CR_PASSWORD) $(CR_HOST)

tag:
ifneq ($(GIT_SHORT_COMMIT_HASH),)
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker tag $(IMAGE_NAME):$(ALPINE_TAG) $(CR_IMAGE_NAME):$(ALPINE_TAG);)
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker tag $(IMAGE_NAME):$(ALPINE_TAG)-$(GIT_SHORT_COMMIT_HASH) $(CR_IMAGE_NAME):$(ALPINE_TAG)-$(GIT_SHORT_COMMIT_HASH);)
else
	$(foreach ALPINE_TAG,$(ALPINE_TAGS),docker tag $(IMAGE_NAME):$(ALPINE_TAG)-dirty $(CR_IMAGE_NAME):$(ALPINE_TAG)-dirty;)
endif
	docker tag $(IMAGE_NAME):latest $(CR_IMAGE_NAME):latest
