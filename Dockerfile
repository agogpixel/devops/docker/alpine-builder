################################################################################
# Global Arguments
################################################################################
ARG ALPINE_TAG

################################################################################
# Builder Image
################################################################################
FROM alpine:${ALPINE_TAG}

# Setup builder environment with common build packages and utilities.
RUN echo http://dl-cdn.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
  echo http://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories && \
  apk update && \
  apk add --no-cache \
    alpine-sdk \
    argp-standalone \
    autoconf \
    automake \
    bash \
    binutils-dev \
    bison \
    bsd-compat-headers \
    build-base \
    bzip2-dev \
    cmake \
    curl-dev \
    elfutils-dev \
    flex-dev \
    fts \
    fts-dev \
    g++ \
    gcc \
    libtool \
    ninja \
    python \
    python3 \
    su-exec \
    xz-dev \
    zlib-dev

# Default work directory.
WORKDIR /usr/local/src

# Default command.
CMD ["bash"]
