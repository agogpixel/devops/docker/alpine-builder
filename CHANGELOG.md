# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Latest]

### Added

- Builder image:
    - WORKDIR: `/usr/local/src`
    - Packages:
        - alpine-sdk
        - argp-standalone
        - autoconf
        - automake
        - bash
        - binutils-dev
        - bison
        - bsd-compat-headers
        - build-base
        - bzip2-dev
        - cmake
        - curl-dev
        - elfutils-dev
        - flex-dev
        - fts
        - fts-dev
        - g++
        - gcc
        - libtool
        - ninja
        - python
        - python3
        - su-exec
        - xz-dev
        - zlib-dev

[Latest]: https://gitlab.com/agogpixel/devops/docker/alpine-builder
