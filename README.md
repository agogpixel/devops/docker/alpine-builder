# alpine-builder

Basic default builder image for use with multi-stage Alpine based Docker definitions.

## Development

- Project utilizes [GitHub flow](https://guides.github.com/introduction/flow/).
- Images built and released to project's [container registry](https://gitlab.com/agogpixel/devops/docker/alpine-builder/container_registry).
